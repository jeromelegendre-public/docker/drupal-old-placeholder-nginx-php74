FROM php:7.4-fpm-alpine3.12

LABEL Maintainer="Jerome Legendre <jerome@legendre.me>" \
      Description="Stack WebApp legere Nginx, Php-fpm (credit a https://github.com/docker-library/drupal)"

ENV php_user www-data
ARG path_project_root=$path_project_root
ARG path_project_webroot=$path_project_webroot



ENV fpm_conf /usr/local/etc/php-fpm.d/www.conf
ENV php_vars /usr/local/etc/php/conf.d/docker-vars.ini

COPY start.sh /start.sh
RUN chmod 755 /start.sh

RUN apk update

RUN set -eux; \
	apk add --no-cache --virtual .build-deps $PHPIZE_DEPS \
    gcc \
    g++ \
    autoconf \
    tar \
		coreutils \
		curl-dev \
		freetype-dev \
		gettext-dev \
		musl-dev \
		libjpeg-turbo-dev \
		libpng-dev \
		libzip-dev \
		libxml2-dev \
# postgresql-dev is needed for https://bugs.alpinelinux.org/issues/3642
		postgresql-dev \
  && apk add --no-cache \
		icu-dev \
		libcurl \
		libxml2 \
#		libintl \
		supervisor \
		git \
		mariadb-client \
		nginx \
		python3 \
		nodejs \
		npm \
		yarn \
		msmtp \
		nano \
		nano-syntax;

  # Composer 1
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php --1 --install-dir=/usr/local/bin --filename=composer && \
    php -r "unlink('composer-setup.php');";

RUN docker-php-ext-configure gd \
		--with-freetype \
		--with-jpeg=/usr/include ;
RUN	docker-php-ext-configure intl
#RUN	docker-php-ext-configure intl && docker-php-ext-install intl && docker-php-ext-enable intl \

RUN docker-php-ext-install \
		intl \
		exif \
		gd \
		gettext \
		iconv \
		opcache \
		pdo_mysql \
		pdo_pgsql \
		zip \
		;



#RUN	runDeps="$( \
#		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local \
#			| tr ',' '\n' \
#			| sort -u \
#			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
#	)" \
#	&& apk add --no-network --virtual $runDeps \

RUN apk del .build-deps;

RUN	{ \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini ;

COPY supervisord.conf /etc/supervisord.conf
COPY nginx.default.conf /etc/nginx/conf.d/default.conf

RUN { \
        mkdir -p /var/log/supervisor ;\
        mkdir -p /run/nginx  ;\
        chown nginx /run/nginx ;\
        export FIND="root \/var\/www\/html"  ;\
        export REPLACE="root \/var\/www\/web" ;\
        sed -i "s/$FIND/$REPLACE/" /etc/nginx/conf.d/default.conf ;\
    }

WORKDIR /var/www
EXPOSE 80

#COPY --chown=www-data:www-data index.php /var/www/html/index.php
COPY app /var/www
RUN chmod -R -w "/var/www/web/sites/default"

CMD ["/start.sh"]
