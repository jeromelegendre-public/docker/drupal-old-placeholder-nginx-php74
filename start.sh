#!/bin/sh

# Set php.ini
if [ ! -z "$SET_PHP_INI_ENV" ]; then
	if [ -f "/usr/local/etc/php/php.ini-${SET_PHP_INI_ENV}" ]; then
		cp -f "/usr/local/etc/php/php.ini-${SET_PHP_INI_ENV}" /usr/local/etc/php/php.ini
    fi
fi


# Increase the memory_limit
if [ ! -z "$PHP_MEM_LIMIT" ]; then
 sed -i "s/memory_limit = 512M/memory_limit = ${PHP_MEM_LIMIT}M/g" /usr/local/etc/php/conf.d/docker-vars.ini
fi

# Increase the post_max_size
if [ ! -z "$PHP_POST_MAX_SIZE" ]; then
 sed -i "s/post_max_size = 100M/post_max_size = ${PHP_POST_MAX_SIZE}M/g" /usr/local/etc/php/conf.d/docker-vars.ini
fi

# Increase the upload_max_filesize
if [ ! -z "$PHP_UPLOAD_MAX_FILESIZE" ]; then
 sed -i "s/upload_max_filesize = 30M/upload_max_filesize= ${PHP_UPLOAD_MAX_FILESIZE}M/g" /usr/local/etc/php/conf.d/docker-vars.ini
fi


private_files=/var/www/files_private
public_files=/var/www/web/sites/default/files

# Set Drupal permission
chown -R php_user $private_files
chown -R php_user $public_files
chmod -R u+w $private_files
chmod -R u+w $public_files


# Start supervisord and services
exec /usr/bin/supervisord -c /etc/supervisord.conf
